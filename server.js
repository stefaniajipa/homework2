const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let products = [
    {
        id: 0,
        productName: 'Samsung Galaxy S9',
        price: 4000
    },
    {
        id: 1,
        productName: 'Iphone XR',
        price: 3999
    }
];

app.get('/get-all', (req, res) => {
    res.status(200).send(products);
})

app.post('/add', (req, res) => {
    if(req.body.productName && req.body.price){
        let product = {
            id: products.length,
            productName: req.body.productName,
            price: req.body.price
        };
        products.push(product);
        res.status(200).send(product);
    } else {
        res.status(500).send('Error!');
    }
});

app.put('/update/:id',(req,res)=>{
    let upd=false;
    products.forEach((product)=>{
        if(product.id==req.params.id){
            product.productName=req.body.productName;
            product.price=req.body.price;
            upd=true;
        }
    });
    if(upd){
        res.status(200).send('Updated');
    }else{
        res.status(404).send('ERROR: resource not found');
    }
});

app.delete('/delete',(req,res)=>{
    let dlt=false;
    for(let i=0;i<products.length;i++){
        if(req.body.productName==products[i].productName){
            products.splice(i,1);
            i--;
            dlt=true;
        }
    }
    if(dlt){
        res.status(200).send(`Deleted: ${req.body.productName}`);
    }else{
        res.status(404).send(`ERROR: ${req.body.productName} not found`);
    }
});

app.listen(8080, () => {
    console.log('Server started on port 8080...');
});